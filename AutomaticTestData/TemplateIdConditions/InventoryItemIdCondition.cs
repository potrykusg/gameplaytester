﻿using EventHorizon.Data;
using Newtonsoft.Json;
using System.Linq;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// All supported states of Inventory item to test
    /// </summary>
    public enum InventoryItemExpectedState
    {
        None,
        NotExist,
        Exist
    }

    /// <summary>
    ///  Test conditon of <see cref="ItemTemplateData"/>
    /// </summary>
    public class InventoryItemIdCondition : BaseIdCondition<InventoryItemExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != InventoryItemExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != InventoryItemExpectedState.None; } }

        public InventoryItemIdCondition(TemplateIdTestData testData)
            : base(testData)
        {
        }

        [JsonConstructor]
        protected InventoryItemIdCondition()
        {
        }

        protected override bool ValidateCondition(InventoryItemExpectedState condition)
        {
            var item = Inventory.Items.FirstOrDefault(_ => _.TemplateId == (UniqueId)TemplateId);        

            if (item == null)
            {
                var uniqueItemId = (UniqueId)TemplateId;
                item = TryGetUniqueItem(uniqueItemId);
            }

            if (item == null)
            {
                return condition == InventoryItemExpectedState.NotExist;
            }

            TestLogger.LogImportant("Item " + item.GetTranslatedFullName(TranslateService) + " should " + condition + " in inventory");

            switch (condition)
            {
                case InventoryItemExpectedState.Exist:
                    return item != null;
                case InventoryItemExpectedState.NotExist:
                    return item == null;
                default:
                    var errorMessage = "Expected " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }

        ItemData TryGetUniqueItem(UniqueId uniqueItemId)
        {
            ItemData uniqueItemData;
            if (GameDataStorage.Ref.TemplatesData.ItemsDrafts.TryGetValue(uniqueItemId, out uniqueItemData))
            {
                return Inventory.Items.FirstOrDefault(_ => _.TemplateId == uniqueItemData.TemplateId);
            }
            return null;
        }
    }
#endif
}
