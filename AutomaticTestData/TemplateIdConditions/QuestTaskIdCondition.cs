﻿using Newtonsoft.Json;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// All possible expected states of <see cref="TaskData"/>
    /// </summary>
    public enum QuestTaskExpectedState
    {
        None,
        NotAdded,
        Active,
        Completed,
        ActiveOrCompleted
    }

    /// <summary>
    /// Test condition of <see cref="TaskTemplateData"/>
    /// </summary>
    public class QuestTaskIdCondition : BaseIdCondition<QuestTaskExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != QuestTaskExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != QuestTaskExpectedState.None; } }

        public QuestTaskIdCondition(TemplateIdTestData testData) 
            : base(testData)
        {
        }

        [JsonConstructor]
        protected QuestTaskIdCondition()
        {
        }

        protected override bool ValidateCondition(QuestTaskExpectedState condition)
        {
            var taskTemplate = QuestManager.TryGetTaskTemplate((UniqueId)TemplateId);

            if (taskTemplate == null)
            {
                TestLogger.LogError("Can't validate task, because template with id " + TemplateId + ", doesn't exist.");
                return false;
            }

            TestLogger.LogImportant("Quest task condition " + Name + " Task \"" + TranslateService.Translate(taskTemplate.TKName) + "\" should be " + condition);

            var quest = QuestManager.TryGetOpenedQuest(taskTemplate.QuestTemplate);

            if (quest == null)
            {
                return condition == QuestTaskExpectedState.NotAdded;
            }

            var task = quest.TryGetTask(taskTemplate);

            if (task == null)
            {
                return condition == QuestTaskExpectedState.NotAdded;
            }

            switch (condition)
            {
                case QuestTaskExpectedState.ActiveOrCompleted:
                    return true;
                case QuestTaskExpectedState.Active:
                    return !task.IsCompleted;
                case QuestTaskExpectedState.Completed:
                    return task.IsCompleted;
                case QuestTaskExpectedState.NotAdded:
                    return false;
                default:
                    var errorMessage = "Expected task state " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }
    }
#endif
}
