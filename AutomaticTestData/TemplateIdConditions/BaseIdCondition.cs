﻿using Newtonsoft.Json;
using QuestSystem;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Interface for all <see cref="TemplateIdTestData"/> conditions. 
    /// </summary>
    public interface ITemplateIdCondition
    {
        void Init(QuestManager questManager, TranslateService translateService, Inventory inventory, PartyManager partyManager);
        string TemplateId { get; }
        bool HasPostCondition { get; }
        bool HasPreCondition { get; }
        bool ValidatePreCondition();
        bool ValidatePostCondition();
    }

    /// <summary>
    /// Base class for all <see cref="ITemplateIdCondition"/>s which can be tested in particular <see cref="TestCase"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseIdCondition<T> : ITemplateIdCondition
    {
        [JsonProperty]
        public T PreCondition { get; set; }

        [JsonProperty]
        public T PostCondition { get; set; }

        [JsonProperty]
        public string TemplateId { get; private set; }

        public abstract bool HasPostCondition { get; }
        public abstract bool HasPreCondition { get; }

        public BaseIdCondition()
        {
        }

        public BaseIdCondition(TemplateIdTestData testData)
        {
            TemplateId = testData.TemplateId;
            Name = testData.Name;
        }

        public void Init(QuestManager questManager, TranslateService translateService, Inventory inventory, PartyManager partyManager)
        {
            QuestManager = questManager;
            TranslateService = translateService;
            Inventory = inventory;
            PartyManager = partyManager;
        }

        public bool ValidatePreCondition()
        {
            return ValidateCondition(PreCondition);
        }

        public bool ValidatePostCondition()
        {
            return ValidateCondition(PostCondition);
        }

        protected string Name { get; private set; }
        protected abstract bool ValidateCondition(T condition);

        protected QuestManager QuestManager { get; private set; }
        protected TranslateService TranslateService { get; private set; }
        protected Inventory Inventory { get; private set; }
        protected PartyManager PartyManager { get; private set; }
    }
#endif
}
