﻿using Newtonsoft.Json;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    ///  All supported states of quest to test
    /// </summary>
    public enum QuestExpectedState
    {
        None,
        NotAdded,
        Active,
        Completed
    }

    /// <summary>
    /// Test condition of <see cref="QuestTemplateData"/>
    /// </summary>
    public class QuestIdCondition : BaseIdCondition<QuestExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != QuestExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != QuestExpectedState.None; } }

        public QuestIdCondition(TemplateIdTestData testData) 
            : base(testData)
        {
        }

        [JsonConstructor]
        protected QuestIdCondition()
        {
        }

        protected override bool ValidateCondition(QuestExpectedState condition)
        {
            var questTemplate = QuestManager.TryGetQuestTemplate((UniqueId)TemplateId);

            if (questTemplate == null)
            {
                TestLogger.LogError("Can't validate quest, because quest template with id " + TemplateId + ", doesn't exist.");
                return false;
            }

            TestLogger.LogImportant("Quest " + Name + " \"" + TranslateService.Translate(questTemplate.TKName) + "\" should be " + condition);

            var quest = QuestManager.TryGetOpenedQuest(questTemplate);

            if (quest == null)
            {
                return condition == QuestExpectedState.NotAdded;
            }

            switch (condition)
            {
                case QuestExpectedState.Active:
                    return !quest.IsCompleted;
                case QuestExpectedState.Completed:
                    return quest.IsCompleted;
                case QuestExpectedState.NotAdded:
                    return false;
                default:
                    var errorMessage = "Expected " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }
    }
#endif
}
