﻿using Newtonsoft.Json;
using System.Linq;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// All supported states of unit template to test
    /// </summary>
    public enum UnitExpectedState
    {
        None,
        Unlocked,
        Locked
    }

    /// <summary>
    /// Test condition of unit template id
    /// </summary>
    public class UnitIdCondition : BaseIdCondition<UnitExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != UnitExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != UnitExpectedState.None; } }

        public UnitIdCondition(TemplateIdTestData testData)
            : base(testData)
        {
        }

        [JsonConstructor]
        protected UnitIdCondition()
        {
        }

        protected override bool ValidateCondition(UnitExpectedState condition)
        {
            var unitTemplate = PartyManager.AllHeroes.FirstOrDefault(_ => _.TemplateId == (UniqueId)TemplateId);

            if (unitTemplate == null)
            {
                TestLogger.LogError("Can't validate unit, because hero with id " + TemplateId + ", doesn't exist.");
                return false;
            }

            TestLogger.LogImportant("Unit " + TranslateService.Translate(unitTemplate.TKName) + " should be " + condition);

            switch (condition)
            {
                case UnitExpectedState.Locked:
                    return unitTemplate.IsLocked;
                case UnitExpectedState.Unlocked:
                    return !unitTemplate.IsLocked;
                default:
                    var errorMessage = "Expected unit state " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }
    }
#endif
}
