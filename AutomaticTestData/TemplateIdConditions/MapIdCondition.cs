﻿using Newtonsoft.Json;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// All supported states of exploration maps
    /// </summary>
    public enum ExplorationMapExpectedState
    {
        None,
        Locked,
        Unlocked
    }

    /// <summary>
    ///  Test conditon of <see cref="MapStateTemplateData"/>
    /// </summary>
    public class MapIdCondition : BaseIdCondition<ExplorationMapExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != ExplorationMapExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != ExplorationMapExpectedState.None; } }

        public MapIdCondition(TemplateIdTestData testData)
            : base(testData)
        {
        }

        [JsonConstructor]
        protected MapIdCondition()
        {
        }

        protected override bool ValidateCondition(ExplorationMapExpectedState condition)
        {
            MapStateTemplateData mapTemplateData;

            if (!GameDataStorage.Ref.GameplayTemplatesData.Maps.TryGetValue((UniqueId)TemplateId, out mapTemplateData))
            {
                return false;
            }

            var map = MapStateService.Ref.GetMapData(mapTemplateData.Name);

            if (map == null)
            {
                return false;
            }

            TestLogger.LogImportant("Map " + TranslateService.Translate(map.TKName) + " should be " + condition);

            switch (condition)
            {
                case ExplorationMapExpectedState.Locked:
                    return !map.IsUnlocked;
                case ExplorationMapExpectedState.Unlocked:
                    return map.IsUnlocked;
                default:
                    var errorMessage = "Expected " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }
    }
#endif
}
