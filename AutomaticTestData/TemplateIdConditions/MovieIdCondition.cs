﻿using Data.Movie;
using Newtonsoft.Json;
using System.Linq;

namespace Data.AutomaticTestData.TemplateIdConditions
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// All supported states of Movie to test
    /// </summary>
    public enum MovieExpectedState
    {
        None,
        Undiscovered,
        Discovered
    }

    /// <summary>
    ///  Test conditon of <see cref="MovieKey"/>
    /// </summary>
    public class MovieIdCondition : BaseIdCondition<MovieExpectedState>
    {
        public override bool HasPostCondition { get { return PostCondition != MovieExpectedState.None; } }
        public override bool HasPreCondition { get { return PreCondition != MovieExpectedState.None; } }

        public MovieIdCondition(TemplateIdTestData testData)
            : base(testData)
        {
        }

        [JsonConstructor]
        protected MovieIdCondition()
        {
        }

        protected override bool ValidateCondition(MovieExpectedState condition)
        {
            var gameDataStorage = GameDataStorage.Ref; //TODO: Inject and move to context

            MovieKey movieKey;
            if (!EnumExt.TryParse(TemplateId, out movieKey))
            {
                TestLogger.LogError("Movie " + movieKey + "\" doesn't exist");
                return false;
            }

            bool isMovieDiscovered = gameDataStorage.GameState.DiscoveredMovies.Contains(movieKey);

            TestLogger.LogImportant("Movie " + movieKey + "\" should be " + condition);

            switch (condition)
            {
                case MovieExpectedState.Discovered:
                    return isMovieDiscovered;
                case MovieExpectedState.Undiscovered:
                    return !isMovieDiscovered;
                default:
                    var errorMessage = "Expected " + condition + " is not supported";
                    TestLogger.LogError(errorMessage);
                    return false;
            }
        }
    }
#endif
}
