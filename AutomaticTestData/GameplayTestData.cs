﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Data.AutomaticTestData;
using UnityEngine;

namespace AutomaticExplorationTest
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Holds all  <see cref="TestCaseData"/>"/>s belonging to current <see cref="GameplayTest"/>
    /// </summary>
    public class GameplayTestData : BaseUniqueObject
    {
        [JsonProperty]
        public string Name { get; private set; }

        [JsonProperty]
        public MapName MapName { get; private set; }

        [JsonProperty]
        public bool IsValid { get; set; }

        [JsonProperty]
        public IEnumerable<LogMessage> Logs { get; set; }

        [JsonProperty]
        public bool IsCompleted { get; set; }

        [JsonProperty]
        public int Order { get; set; }

        /// <summary>
        /// Tests which would be impossible to execute during the same game approach conflicts with each other and should have different pass id.
        /// Example: TestCindrosA, TestCindrosB - these tests includes the same events and game state should be reseted between executing them.
        /// </summary>
        [JsonProperty]
        public int PassId
        {
            get { return _passId; }
            set
            {
                _prevPassId = _passId;
                _passId = value;
            }
        }

        [JsonProperty]
        public bool ShouldBeProcessed { get; set; }

        public IEnumerable<TestCaseData> TestCasesData { get { return _testCases.EnsureNotNull(); } }
        public bool HasName { get { return !Name.IsNullOrEmpty(); } }

        public GameplayTestData(UniqueObjectDesc desc, string name, MapName mapName, GameDataStorage gameDataStorage)
            : base(desc)
        {
            PassId = 0;
            Name = name;
            MapName = mapName;
            IsValid = false;
            ShouldBeProcessed = true;
            SetOrderToLastFromCurrentPassId(gameDataStorage);
        }

        public bool TrySwapTestCases(TestCaseData a, TestCaseData b)
        {
            return _testCases.TrySwap(a, b);
        }

        public void AddTestCase(TestCaseData testCase)
        {
            NewUtil.CreateIfNull(ref _testCases);
            testCase.Register(this);
            _testCases.Add(testCase);
        }

        public bool TryRemoveTestCase(TestCaseData testCaseData)
        {
            if (_testCases.IsNullOrEmpty())
            {
                return false;
            }

            testCaseData.Unregister(this);

            return _testCases.Remove(testCaseData);
        }

        public void Remove(GameDataStorage gameDataStorage)
        {
            DecrementOrderForTestsWithHigherOneFromCurrentPassId(gameDataStorage);
            RemoveTestCases();
            gameDataStorage.TestsTemplatesData.Tests.Remove(Id);
        }

        public void OnDataChanged(GameDataStorage gameDataStorage, bool isPassIdChanged = false)
        {
            Debug.Log("Test " + Name + " has changed.");

            if (isPassIdChanged)
            {
                SetOrderToLastFromCurrentPassId(gameDataStorage, isPassIdChanged);
            }

            IsCompleted = false;
        }

        protected GameplayTestData(UniqueObjectDesc desc)
        : base(desc)
        {
        }

        protected override void OnAfterDeserialization()
        {
            base.OnAfterDeserialization();
            RegisterAllTestCases();
        }

        [JsonProperty("TestCases")]
        List<TestCaseData> _testCases;

        int _prevPassId;
        int _passId;

        void DecrementOrderForTestsWithHigherOneFromCurrentPassId(GameDataStorage gameDataStorage)
        {
            var testTemplates = gameDataStorage.TestsTemplatesData;

            var testsWithHigherOrderFromCurrentPassId = testTemplates.Tests.Values
                .Where(_ => _.MapName == MapName)
                .Where(_ => _.PassId == PassId)
                .OrderByDescending(_ => _.Order)
                .Where(_ => _.Order > Order);

            foreach (var test in testsWithHigherOrderFromCurrentPassId)
            {
                test.Order--;
            }
        }

        void SetOrderToLastFromCurrentPassId(GameDataStorage gameDataStorage, bool isPassIdChanged = false)
        {
            if (isPassIdChanged)
            {
                int tempPassId = PassId;
                PassId = _prevPassId;
                DecrementOrderForTestsWithHigherOneFromCurrentPassId(gameDataStorage);
                PassId = tempPassId;
            }

            int highestOrderNumberFromCurrentTestVersion = gameDataStorage.TestsTemplatesData.Tests
                .Where(_ => _.Value.MapName == MapName)
                .Where(_ => _.Value.PassId == PassId && _.Value != this)
                .Select(_ => _.Value.Order)
                .DefaultIfEmpty()
                .Max();

            Order = highestOrderNumberFromCurrentTestVersion + 1;
        }

        void RemoveTestCases()
        {
            if (_testCases.IsNullOrEmpty())
            {
                return;
            }

            foreach (var testCase in _testCases.ToList())
            {
                TryRemoveTestCase(testCase);
            }
        }

        void RegisterAllTestCases()
        {
            if (!_testCases.IsNullOrEmpty())
            {
                foreach (var testCase in _testCases)
                {
                    testCase.Register(this);
                }
            }
        }
    }
#endif
}

