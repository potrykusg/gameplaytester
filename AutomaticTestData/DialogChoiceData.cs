﻿using System.Collections.Generic;
using System.Linq;
using AutomaticExplorationTest;
using Newtonsoft.Json;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Data.AutomaticTestData
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Contains <see cref="DialogChoiceData"/>s which should be automatically chosen during particular <see cref="TestCase"/>
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DialogChoiceData 
    {
        [JsonProperty]
        public string DialogId
        {
            get { return _dialogId; }
            set
            {
                _dialogId = value;
                OnDialogIdChanged();
            }
        }

        [JsonProperty]
        public string ChoiceId { get; set; }

        [JsonProperty]
        public string ChoiceTKkey { get; set; }

        [JsonProperty]
        public int? PhraseId { get; set; }

        public TestCaseData Test { get; private set; }

        public IEnumerable<string> AvailableChoicesIds
        {
            get { return _possibleChoices; }
            private set
            {
                _possibleChoices.Clear();
                _possibleChoices.AddRange(value);
            }
        }

        public DialogChoiceData()
        {
            DialogId = "";
            ChoiceTKkey = "";
            ChoiceId = "";
        }

        string _dialogId;
        readonly List<string> _possibleChoices = new List<string>();

        void OnDialogIdChanged()
        {
            DialogTemplateData dialogTemplate;
            GameDataStorage.Ref.GameplayTemplatesData.Dialogs.TryGetValue((UniqueId)DialogId, out dialogTemplate);

            if (dialogTemplate == null)
            {
                return;
            }

#if UNITY_EDITOR
            if (Test != null && dialogTemplate.MapId != Test.MapName)
            {
                EditorUtility.DisplayDialog("Dialog from wrong map",
                    "Chosen dialog " + dialogTemplate.Id + " belongs to map " + dialogTemplate.MapId +
                    ", but current test belongs to map." + Test.MapName + ". Assign dialog from appropiate map.", "ok");

                DialogId = "";
                return;
            }
#endif

            AvailableChoicesIds = dialogTemplate.Phrases.SelectMany(_ => _.Choices).Select(_ => _.Id);
        }
    }
#endif
}
