﻿using Data.AutomaticTestData;
using AutomaticExplorationTest;
using Newtonsoft.Json;
using UnityEngine;

#if UNITY_EDITOR || ENABLE_TESTS
/// <summary>
/// Immutable info about automatic tests
/// </summary>
public class TestsTemplatesData : BaseUniqueObject
{
    [JsonProperty]
    public UniqueObjDict<GameplayTestData> Tests = new UniqueObjDict<GameplayTestData>();

    [JsonProperty]
    public UniqueObjDict<TestCaseData> TestCases = new UniqueObjDict<TestCaseData>();

    [JsonProperty]
    public UniqueObjDict<MapEventTestData> MapEventsTests = new UniqueObjDict<MapEventTestData>();

    [JsonProperty]
    public UniqueObjDict<TemplateIdTestData> TemplateIdTests = new UniqueObjDict<TemplateIdTestData>();

    [JsonProperty]
    public bool TriggerTestRun;

    [JsonProperty]
    public int CurrentlyProcessedTestsVersion;

    public TestsTemplatesData(UniqueObjectDesc desc)
        : base(desc)
    {
    }

    public TestsTemplatesData()
        : base(new UniqueObjectDesc(typeof(TestsTemplatesData), (UniqueId)"$root_tests$"))
    {
    }

    public bool SwapTestsOrder(GameplayTestData a, GameplayTestData b)
    {
        if (a.PassId != b.PassId)
        {
            Debug.LogError("Failed to swap test " + a.Name + " with " + b.Name + ", because their pass ids are not the same");
            return false;
        }

        int tempOrder = a.Order;
        a.Order = b.Order;
        b.Order = tempOrder;

        return true;
    }
}
#endif

