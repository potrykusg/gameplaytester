﻿using System.Linq;
using AutomaticExplorationTest;
using EventHorizon.GameServices;
using EventHorizon.Exploration.Restorables;
using Newtonsoft.Json;
using UnityEngine;

namespace Data.AutomaticTestData
{
    /// <summary>
    /// All possible reactions of <see cref="IMapEvent"/> after attempt to activation
    /// </summary>
    public enum MapEventExpectedState
    {
        None,
        Activated,
        NotActivated,
        BlockedUse,
        UnblockedUse,
        BlockedSubscribingToUI,
        UnblockedSubscribingToUI,
        ActiveInHierarchy,
        InactiveInHierarchy,
        CanBeActivated,
        CannotBeActivated,
        //TODO: refactor this, it should be separate enum DoorEventExpectedState
        DoorOpened,
        DoorClosed,
        //TODO: refactor this, it should be separate enum TogglableEventExpectedState
        TogglableActive,
        TogglableInactive
    }

#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Contains data for testing <see cref="IMapEvent"/>s. Also validates if state of given <see cref="IMapEvent"/> responds to required one.
    /// </summary>
    public class MapEventTestData : BaseTestCaseConditionData, IDataWithMapEventId //TODO: it should be abstract class BaseEventTestData<TExpectedState> in order to support specific mapEvents tests
    {
        [JsonProperty]
        public UniqueId MapEventId { get; set; }

        [JsonProperty]
        public MapEventExpectedState PreCondition { get; private set; }

        [JsonProperty]
        public MapEventExpectedState PostCondition { get; private set; }

        public override bool HasPreCondition { get { return PreCondition != MapEventExpectedState.None; } }
        public override bool HasPostCondition { get { return PostCondition != MapEventExpectedState.None; } }

        public IMapEvent MapEvent
        {
            get
            {
                if (Application.isPlaying || !_isCachedMapEventDestroyed || _prevMapEventId != MapEventId)
                {
                    MapEventsManager.Ref.UpdateCachedMapEvent(ref _mapEvent, MapEventId);
                }

                _isCachedMapEventDestroyed = _mapEvent == null;
                _prevMapEventId = MapEventId;
                return _mapEvent;
            }
        }

        public MapEventTestData(UniqueObjectDesc desc, MapEventExpectedState preCondition, MapEventExpectedState postCondition, string name, TestCaseData testCase)
            : base(desc)
        {
            PreCondition = preCondition;
            PostCondition = postCondition;
            Name = name;
            IsValid = false;
            testCase.AddNewTestCondition(this);
        }

        protected override bool OnValidatePreCondition()
        {
            return ValidateCondition(PreCondition);
        }

        protected override bool OnValidatePostCondition()
        {
            return ValidateCondition(PostCondition);
        }

        protected override bool OnRemove()
        {
            return GameDataStorage.Ref.TestsTemplatesData.MapEventsTests.Remove(Id);
        }

        protected MapEventTestData(UniqueObjectDesc desc)
            : base(desc)
        {
        }

        protected override bool OnDataUpdate()
        {
            var conditionData = GameDataStorage.Ref.TestsTemplatesData.MapEventsTests.FirstOrDefault(_ => _.Key == Id)
                .Value;

            if (conditionData == null)
            {
                return false;
            }

            conditionData.IsValid = IsValid;
            return true;
        }

        IMapEvent _mapEvent;
        UniqueId _prevMapEventId;
        bool _isCachedMapEventDestroyed;

        bool ValidateCondition(MapEventExpectedState condition)
        {
            if (MapEvent == null)
            {
                TestLogger.LogError("Can't process condition " + Name + ", because testing MapEvent is null.");
                return false;
            }

            TestLogger.LogImportant("Condition " + Name + " MapEvent \"" + MapEvent.gameObject.name + "\" should be " + condition);

            bool isActivated = MapEvent.IsActivated;
            bool isActiveInHierarchy = MapEvent.gameObject.activeInHierarchy;
            bool isBlockedFromUse = MapEvent.IsBlockedFromUse;
            bool isBlockedFromSubscribingToUI = MapEvent.IsBlockedFromSubscribingToUI;
            bool canActivate = MapEvent.CanBeActivated && !MapEvent.IsBlockedFromUse && !MapEvent.IsBlockedFromSubscribingToUI && MapEvent.gameObject.activeInHierarchy;

            //TODO: it should be refactored and moved to another, eventSpecific class
            var doorEvent = MapEvent as DoorEvent;
            if ((condition == MapEventExpectedState.DoorOpened || condition == MapEventExpectedState.DoorClosed) && doorEvent == null)
            {
                TestLogger.LogError("Failed to test door conditions on mapEvent of another type than DoorEvent");
                return false;
            }

            //TODO: it should be refactored and moved to another, eventSpecific class
            var togglableEvent = MapEvent as ITogglableMapEvent;
            if ((condition == MapEventExpectedState.TogglableActive || condition == MapEventExpectedState.TogglableInactive) && togglableEvent == null)
            {
                TestLogger.LogError("Failed to test togglableEvent conditions on mapEvent of another type than ITogglableMapEvent");
                return false;
            }

            switch (condition)
            {
                case MapEventExpectedState.Activated:
                    IsValid = isActivated;
                    break;
                case MapEventExpectedState.ActiveInHierarchy:
                    IsValid = isActiveInHierarchy;
                    break;
                case MapEventExpectedState.BlockedUse:
                    IsValid = isBlockedFromUse;
                    break;
                case MapEventExpectedState.BlockedSubscribingToUI:
                    IsValid = isBlockedFromSubscribingToUI;
                    break;
                case MapEventExpectedState.NotActivated:
                    IsValid = !isActivated;
                    break;
                case MapEventExpectedState.UnblockedUse:
                    IsValid = !isBlockedFromUse;
                    break;
                case MapEventExpectedState.UnblockedSubscribingToUI:
                    IsValid = !isBlockedFromSubscribingToUI;
                    break;
                case MapEventExpectedState.InactiveInHierarchy:
                    IsValid = !isActiveInHierarchy;
                    break;
                case MapEventExpectedState.CanBeActivated:
                    IsValid = canActivate;
                    break;
                case MapEventExpectedState.CannotBeActivated:
                    IsValid = !canActivate;
                    break;
                case MapEventExpectedState.DoorOpened:
                    return doorEvent.Door.IsOpened;
                case MapEventExpectedState.DoorClosed:
                    return !doorEvent.Door.IsOpened;
                case MapEventExpectedState.TogglableActive:
                    return togglableEvent.IsActive;
                case MapEventExpectedState.TogglableInactive:
                    return !togglableEvent.IsActive;
                default:
                    Debug.LogError("Unsupported MapEventExpectedState " + condition);
                    IsValid = false;
                    break;
            }

            return IsValid;
        }
    }
#endif
}
