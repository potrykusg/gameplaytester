﻿using AutomaticExplorationTest;
using EventHorizon.Configuration;
using Newtonsoft.Json;
using QuestSystem;
using System;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Data.AutomaticTestData
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Base class for all conditions of <see cref="TestCaseData"/>. Directly linked, TestCaseCondition can't exist without being linked to particular <see cref="TestCaseData"/>
    /// </summary>
    public abstract class BaseTestCaseConditionData : BaseUniqueObject, ITestConditionData<TestCaseData>
    {
        [JsonProperty]
        public string Name { get; protected set; }

        [JsonProperty]
        public bool IsValid { get; set; }

        [JsonProperty]
        public string PathToSavesFromInvalidValidation { get; private set; }

        public TestCaseData TestCaseData { get; protected set; }

        public abstract bool HasPreCondition { get; }
        public abstract bool HasPostCondition { get; }
        public bool HasSaveFromInvalidValidation { get { return !PathToSavesFromInvalidValidation.IsNullOrEmpty(); } }

        public void Init(GameManager gameManager, GameDataStorage gameDataStorage, FileService fileService, QuestManager questManager,
            TranslateService translateService, Inventory inventory, PartyManager partyManager)
        {
            GameManager = gameManager;
            GameDataStorage = gameDataStorage;
            FileService = fileService;
            QuestManager = questManager;
            TranslateService = translateService;
            Inventory = inventory;
            PartyManager = partyManager;
            OnInit();
        }

        public void Remove()
        {
            FileService.Ref.DeleteDirectory(PathToSavesFromInvalidValidation, recursive: true);

            bool isRemoved = OnRemove();

            if (!isRemoved)
            {
                Debug.LogError("Test case condition " + Name + " with id " + Id + " doesn't exist in game state, so its value can't be removed");
            }
        }

        public bool ValidatePreCondition()
        {
            IsValid = OnValidatePreCondition();

            if (!IsValid)
            {
                SaveGameState(isPostCondition: false);
            }

            UpdateData();

            return IsValid;
        }

        public bool ValidatePostCondition()
        {
            IsValid = OnValidatePostCondition();

            if (!IsValid)
            {
                SaveGameState(isPostCondition: true);
            }

            UpdateData();

            return IsValid;
        }

        void IDataSubscriber<TestCaseData>.Register(TestCaseData testCaseData)
        {
            TestCaseData = testCaseData;
        }

        void IDataSubscriber<TestCaseData>.Unregister(TestCaseData testCaseData)
        {
            if (TestCaseData != testCaseData)
            {
                Debug.LogError("Mismatched test case. Current test is " + TestCaseData.Name + ", tried to unregister from " + testCaseData.Name);
                return;
            }

            TestCaseData = null;

            Remove();
        }

        protected abstract bool OnRemove();
        protected abstract bool OnValidatePreCondition();
        protected abstract bool OnValidatePostCondition();
        protected abstract bool OnDataUpdate();

        protected virtual void OnInit()
        {
        }

        protected BaseTestCaseConditionData(UniqueObjectDesc desc)
            : base(desc)
        {
        }

        protected FileService FileService { get; private set; }
        protected GameDataStorage GameDataStorage { get; private set; }
        protected GameManager GameManager { get; private set; }
        protected QuestManager QuestManager { get; private set; }
        protected TranslateService TranslateService { get; private set; }
        protected Inventory Inventory { get; private set; }
        protected PartyManager PartyManager { get; private set; }

        void UpdateData()
        {
            bool isDataUpdated = OnDataUpdate();

            if (!isDataUpdated)
            {
                Debug.LogError("Test case condition " + Name + " with id " + Id + " doesn't exist in game state, so its value can't be updated.");
            }

            GameDataStorage.SaveTests();
        }

        void SaveGameState(bool isPostCondition)
        {
            string name = FileService.SanitizeFileName(Name);
            string path = TestCaseData.Test.MapName + "/" + TestCaseData.Test.Name + "/" + TestCaseData.Name + "/" + name;
            string rootPath;
#if UNITY_EDITOR
            rootPath = EditorConfig.EditorAutomaticTestSavesPath;
#else
            rootPath = Application.dataPath;
#endif
            string fullPath = Path.Combine(rootPath, path);
            PathToSavesFromInvalidValidation = fullPath;

            TestLogger.Log("Saving game state to " + fullPath);

            string fileNamePrefix = (isPostCondition ? "PostCondition_" : "PreCondition_") + name + "_";
            string fullFileName = fileNamePrefix + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");

            var file = FileService.GetFile(fullFileName, Config.SaveFileExtension, fullPath);

            if (file == null)
            {
                return;
            }

            DeleteDeprecatedSaves(fullPath, fileNamePrefix);
            GameDataStorage.Save(file, GameManager.CurrentGameMode);
        }

        void DeleteDeprecatedSaves(string path, string fileNamePrefix)
        {
            var files = Directory.GetFiles(path);
            var deprecatedFiles = files.Select(Path.GetFileName).Where(_ => _.StartsWith(fileNamePrefix));
            FileService.DeleteFiles(path, deprecatedFiles);
        }
    }
#endif
}
