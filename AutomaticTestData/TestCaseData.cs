﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using EventHorizon.GameServices;
using EventHorizon.Exploration.Restorables;
using UnityEngine;

namespace AutomaticExplorationTest
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Holds data necessary to validate <see cref="TestCase"/>"/>
    /// </summary>
    public class TestCaseData : BaseUniqueObject, IDataWithMapEventId, IDataSubscriber<GameplayTestData>
    {
        [JsonProperty]
        public string Name { get; private set; }

        [JsonProperty]
        public UniqueId MapEventId { get; set; }

        [JsonProperty]
        public bool IsValid { get; set; }

        [JsonProperty]
        public MapName MapName { get; set; }

        [JsonProperty]
        public int RotationSteps { get; set; }

        [JsonProperty]
        public string PinpadCode { get; set; }

        [JsonProperty]
        public Dictionary<UniqueId, List<string>> _choicesByDialog { get; private set; }

        public IMapEvent MapEvent
        {
            get
            {
                if (Application.isPlaying || !_isCachedMapEventDestroyed || _prevMapEventId != MapEventId)
                {
                    MapEventsManager.Ref.UpdateCachedMapEvent(ref _mapEvent, MapEventId);
                }
                _isCachedMapEventDestroyed = _mapEvent == null;
                _prevMapEventId = MapEventId;
                return _mapEvent;
            }
        }

        public GameplayTestData Test { get; private set; }

        public IEnumerable<ITestConditionData<TestCaseData>> TestConditionsData
        {
            get { return _testConditionsData.EnsureNotNull(); }
        }

        public IEnumerable<Data.AutomaticTestData.DialogChoiceData> DialogChoices
        {
            get { return _dialogChoices.EnsureNotNull(); }
        }

        public bool HasMapEventId
        {
            get { return !MapEventId.IsValid; }
        }

        public bool HasName
        {
            get { return !Name.IsNullOrEmpty(); }
        }

        public TestCaseData(UniqueObjectDesc desc, string name, GameplayTestData test)
            : base(desc)
        {
            Name = name;
            MapName = test.MapName;
            _choicesByDialog = new Dictionary<UniqueId, List<string>>();
            test.AddTestCase(this);
        }

        public void AddDialogChoice(Data.AutomaticTestData.DialogChoiceData dialogChoiceData)
        {
            NewUtil.CreateIfNull(ref _dialogChoices);
            _dialogChoices.Add(dialogChoiceData);
        }

        public void AddNewTestCondition(ITestConditionData<TestCaseData> testConditionTemplateData)
        {
            NewUtil.CreateIfNull(ref _testConditionsData);
            testConditionTemplateData.Register(this);
            _testConditionsData.Add(testConditionTemplateData);
        }

        public void Register(GameplayTestData test)
        {
            Test = test;
        }

        public void Unregister(GameplayTestData test)
        {
            if (Test != test)
            {
                Debug.LogError("Mismatched test. Tried to unregister from test " + test.Name +
                               ", but current test is " + Test);
                return;
            }

            RemoveAllConditions();

            if (!Remove())
            {
                Debug.LogError("Couldn't remove test case " + Name + " with id " + Id);
            }

            Test = null;
        }

        public bool TryRemoveTestCondition(ITestConditionData<TestCaseData> testConditionData)
        {
            if (_testConditionsData.IsNullOrEmpty())
            {
                return false;
            }

            testConditionData.Unregister(this);

            return _testConditionsData.Remove(testConditionData);
        }

        public bool TryRemoveDialogChoice(Data.AutomaticTestData.DialogChoiceData dialogChoice)
        {
            if (_dialogChoices.IsNullOrEmpty())
            {
                return false;
            }

            return _dialogChoices.Remove(dialogChoice);
        }

        public bool TryRemoveDialogChoice(int dialogChoiceId)
        {
            if (_dialogChoices.IsNullOrEmpty()
                || (dialogChoiceId < 0)
                || (dialogChoiceId >= _dialogChoices.Count))
            {
                return false;
            }
            var choice  = _dialogChoices[dialogChoiceId];
            return TryRemoveDialogChoice(choice);
        }

        protected TestCaseData(UniqueObjectDesc desc)
            : base(desc)
        {
        }

        protected override void OnAfterDeserialization()
        {
            base.OnAfterDeserialization();
            RegisterAllConditions();
            RemoveEmptyChoices();
        }

        IMapEvent _mapEvent;
        UniqueId _prevMapEventId;
        bool _isCachedMapEventDestroyed;

        [JsonProperty("TaskTemplates")]
        List<ITestConditionData<TestCaseData>> _testConditionsData;

        [JsonProperty("DialogChoices")]
        List<Data.AutomaticTestData.DialogChoiceData> _dialogChoices;

        void RemoveEmptyChoices()
        {
            if (_dialogChoices.IsNullOrEmpty())
            {
                return;
            }

            _dialogChoices.RemoveAll(_ => _.ChoiceId.IsNullOrEmpty());
        }

        void RemoveDialogChoices()
        {
            if (_dialogChoices.IsNullOrEmpty())
            {
                return;
            }

            _dialogChoices.Clear();
        }

        bool Remove()
        {
            RemoveDialogChoices();
            return GameDataStorage.Ref.TestsTemplatesData.TestCases.Remove(Id);
        }

        void RemoveAllConditions()
        {
            if (_testConditionsData.IsNullOrEmpty())
            {
                return;
            }

            foreach (var condition in _testConditionsData.ToList())
            {
                if (!TryRemoveTestCondition(condition))
                {
                    Debug.LogError("Coudln't remove condition " + condition.Name);
                }
            }
        }

        void RegisterAllConditions()
        {
            if (_testConditionsData.IsNullOrEmpty())
            {
                return;
            }

            foreach (var condition in _testConditionsData)
            {
                condition.Register(this);
            }
        }
    }
#endif
}