﻿using AutomaticExplorationTest;
using Data.AutomaticTestData.TemplateIdConditions;
using Newtonsoft.Json;
using System.Linq;
using UnityEngine;

namespace Data.AutomaticTestData
{
#if UNITY_EDITOR || ENABLE_TESTS
    /// <summary>
    /// Contains data for <see cref="ITemplateIdCondition"/> conditions
    /// </summary>
    public class TemplateIdTestData : BaseTestCaseConditionData
    {
        [JsonProperty]
        public ITemplateIdCondition TemplateIdCondition { get; set; }

        [JsonProperty]
        public string TemplateId { get; set; }

        public override bool HasPreCondition { get { return TemplateIdCondition.HasPreCondition; } }
        public override bool HasPostCondition { get { return TemplateIdCondition.HasPostCondition; } }

        public TemplateIdTestData(UniqueObjectDesc desc, string name, TestCaseData testCase)
            : base(desc)
        {
            Name = name;
            testCase.AddNewTestCondition(this);
        }

        protected override void OnInit()
        {
            base.OnInit();
            TemplateIdCondition.Init(QuestManager, TranslateService, Inventory, PartyManager);
        }

        protected override bool OnRemove()
        {
            if (GameDataStorage == null)
            {
                Debug.LogError("GameDataStorage is null");
                return false;
            }
            return GameDataStorage.TestsTemplatesData.TemplateIdTests.Remove(Id);
        }

        protected override bool OnValidatePreCondition()
        {
            IsValid = TemplateIdCondition.ValidatePreCondition();
            return IsValid;
        }

        protected override bool OnValidatePostCondition()
        {
            IsValid = TemplateIdCondition.ValidatePostCondition();
            return IsValid;
        }

        protected TemplateIdTestData(UniqueObjectDesc desc)
            : base(desc)
        {
        }

        protected override bool OnDataUpdate()
        {
            var templateIdTestData = GameDataStorage.TestsTemplatesData.TemplateIdTests.FirstOrDefault(_ => _.Key == Id).Value;

            if (templateIdTestData == null)
            {
                return false;
            }

            templateIdTestData.IsValid = IsValid;
            return true;
        }
    }
#endif
}
