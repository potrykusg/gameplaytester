﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data.AutomaticTestData
{
    /// <summary>
    /// Log message types used by <see cref="TestLogger"/>,
    /// </summary>
    public enum LogMessageType
    {
        Common,
        Important,
        Error,
        Positive
    }

    /// <summary>
    /// Single log message
    /// </summary>
    public class LogMessage
    {
        public LogMessageType Type { get; private set; }
        public string Message { get; private set; }

        public LogMessage(string message, LogMessageType type)
        {
            Message = message;
            Type = type;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    /// <summary>
    /// Test log contains list of <see cref="LogMessage"/>s.
    /// </summary>
    public static class TestLogger
    {
        public static void Log(string message)
        {
            _log.Add(new LogMessage(DateTime.Now.ToString("hh:mm:ss") + " " + message, LogMessageType.Common));
        }

        public static void LogImportant(string message)
        {
            _log.Add(new LogMessage(DateTime.Now.ToString("hh:mm:ss") + " " + message, LogMessageType.Important));
        }

        public static void LogError(string message)
        {
            Debug.LogError(message);
            _log.Add(new LogMessage(DateTime.Now.ToString("hh:mm:ss") + " " + message, LogMessageType.Error));
        }

        public static void LogPositive(string message)
        {
            _log.Add(new LogMessage(DateTime.Now.ToString("hh:mm:ss") + " " + message, LogMessageType.Positive));
        }

        public static void LogErrorOrException(string message, string stackTrace, LogType logType)
        {
            if (logType == LogType.Exception)
            {
                LogError("Exception occured: " + message);

                var stackTraceLines = stackTrace.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var line in stackTraceLines)
                {
                    LogError(line);
                }

            }
            else if (logType == LogType.Error)
            {
                LogError("Error occured: " + message);
            }
        }

        public static void ClearLog()
        {
            _log.Clear();
        }

        public static IEnumerable<LogMessage> GetWholeLog()
        {
            return _log.Select(_ => (LogMessage)_.Clone()).ToList();
        }

        static readonly List<LogMessage> _log = new List<LogMessage>();
    }
}
