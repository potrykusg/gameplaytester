﻿using AutomaticExplorationTest;
using System.Collections.Generic;
using Windows.BasicDrawers.Filters;
using System.Linq;
using UnityEditor;
using UnityEngine;
using EventHorizon.Utils;

/// <summary>
/// Table with <see cref="TestCaseData"/>
/// </summary>
public class TestCasesDataDrawer : BaseTableDrawer<TestCasesDataDrawer, TestCaseData>
{
    public delegate ITableDrawer ChildTreeFactory(TestCasesDataDrawer parent, TestCaseData testCase, IContentFilter filter);

    public TestCasesDataDrawer(TableDrawerDesc desc, GameplayTestData test, GameEditorState state, TestCasesDataDrawer.ChildTreeFactory childTreeFactory)
        : base(desc, _columns, "testCase")
    {
        _state = state;
        _childTreeFactory = childTreeFactory;
        _testData = test;
    }

    public override bool CanSwap(TestCaseData obj) { return true; }

    public override TestCaseData CreateNew()
    {
        var newTestCase = new TestCaseData(
            new UniqueObjectDesc(typeof(TestsTemplatesData), UniqueId.NewFromGuid("testCase_")),
            "TestCase",
            _testData
        );

        GameEditorUtils.ActiveState.Storage.TestsTemplatesData.TestCases.Add(newTestCase);

        return newTestCase;
    }

    public override IEnumerable<TestCaseData> GetItems()
    {
        return _testData.TestCasesData;
    }

    public override void DrawChildren(TestCaseData obj, float width)
    {
        var drawer = GetOrCreateChild(obj);
        drawer.Draw(width);

        var labelColumnWidth = 100;

        EditClassList(ContentRowStartSpace, labelColumnWidth, "Dialog choices", "dialog choice", obj.DialogChoices, obj.TryRemoveDialogChoice,
            createNew: () =>
            {
                obj.AddDialogChoice(new Data.AutomaticTestData.DialogChoiceData());
            },
            editItem: _ =>
            {
                GameEditorUtils.EditString(_, "DialogId", GUILayout.MinWidth(100), NotExpandHeight, NotExpandWidth);
                ChooseChoiceId(obj, _);
                var size = new Vector2(800, 15);
                DrawIdValidation(size, _.DialogId, DialogTemplateIdSelectorDrawer.Ref);
            });

        if (_choiceToRemove != null)
        {
            obj.TryRemoveDialogChoice(_choiceToRemove);
            _choiceToRemove = null;
        }

    }

    protected override bool OnSwap(TestCaseData a, TestCaseData b)
    {
        return _testData.TrySwapTestCases(a, b);
    }

    protected override void OnRemove(TestCaseData obj)
    {
        if (!_testData.TryRemoveTestCase(obj))
        {
            Debug.LogError("Couldn't remove test case" + obj.Name);
        }
    }

    protected override MatchResult IsItemMatching(TestCaseData item, IContentFilter filter)
    {
        if (filter.IsMatching(item.Id)
            || filter.IsMatching(item.Name))
        {
            return MatchResult.Self;
        }

        return GetOrCreateChild(item).HasAnyItemMatching(filter) ? MatchResult.Child : MatchResult.None;
    }

    protected override void OnClearCachedItems()
    {
        _childDrawers.ForEach(_ => _.Value.ClearCachedItems());
    }

    protected override void OnChange(BaseColumn column, TestCaseData testCaseData)
    {
        testCaseData.Test.OnDataChanged(_state.Storage);
    }

    readonly GameEditorState _state;
    readonly GameplayTestData _testData;
    readonly ChildTreeFactory _childTreeFactory;
    readonly Dictionary<TestCaseData, ITableDrawer> _childDrawers = new Dictionary<TestCaseData, ITableDrawer>();
    Data.AutomaticTestData.DialogChoiceData _choiceToRemove = null;

    static readonly BaseColumn[] _columns = new BaseColumn[]
    {
        CreateOrderColumn(),
        CreateDetailRequestColumn(),

        new DefaultColumn(){ Label = "Name", FixedWidth = 200, IsVisibleInDetailView = false, DrawHandler = _ => DrawLabel(_.Value.Name.ToString(), _.Size, BoldLabelStyle) },
        new DefaultColumn(){ Label = "Edit Name", FixedWidth = 200, IsVisibleOnlyInDetailView = false, DrawHandler = _ => EditString(_.Value, "Name", _.Size)},
        new DefaultColumn(){ Label = "Map", FixedWidth = 200, IsVisibleInDetailView = true, ShouldNotifyAboutChange = true, DrawHandler = _ => EditEnum(_.Value, "MapName", GameEditorUtils.ExplorationMaps, GameEditorUtils.ExplorationMapsLabels, _.Size ) },
        new DefaultColumn(){ Label = "MapEvent", FixedWidth = 200, IsVisibleInDetailView = true, ShouldNotifyAboutChange = true, DrawHandler = _ => DrawMapEventField(_.Value, _.Value.MapName, _.Size, isTestCaseTarget: true) },
        new DefaultColumn(){ Label = "IsValid", FixedWidth = 200, IsVisibleOnlyInDetailView = true, DrawHandler = _ => DrawLabel(_.Value.IsValid.ToString(), _.Size, LabelStyle) },
    };

    ITableDrawer GetOrCreateChild(TestCaseData test)
    {
        return _childDrawers.GetOrCreate(test, () => _childTreeFactory(this, test, ContentFilter));
    }

    void ChooseChoiceId(TestCaseData testcaseData, Data.AutomaticTestData.DialogChoiceData dialogChoiceData)
    {
        int id = dialogChoiceData.AvailableChoicesIds.ToArray().IndexOf(_ => _ == dialogChoiceData.ChoiceId);
        int prevId = id;

        id = EditPopup(id, dialogChoiceData.AvailableChoicesIds.ToArray(), NotExpandWidth, NotExpandHeight, GUILayout.MinWidth(100));

        if (prevId != id)
        {
            testcaseData.Test.OnDataChanged(_state.Storage);
        }

        if (!dialogChoiceData.AvailableChoicesIds.IsNullOrEmpty() && id >= 0)
        {
            var chosenChoiceId = dialogChoiceData.AvailableChoicesIds.ToArray()[id];

            DialogTemplateData dialogTemplate;
            GameEditorUtils.ActiveState.Storage.GameplayTemplatesData.Dialogs.TryGetValue((UniqueId)dialogChoiceData.DialogId, out dialogTemplate);

            if (dialogTemplate == null)
            {
                return;
            }

            var chosenChoicePhrase = dialogTemplate.Phrases.FirstOrDefault(phrase => phrase.Choices.Any(_ => _.Id == chosenChoiceId));

            dialogChoiceData.ChoiceId = chosenChoiceId;
            dialogChoiceData.ChoiceTKkey = dialogTemplate.GetChoiceTK(new DialogChoiceData(chosenChoiceId)).Key;
            dialogChoiceData.PhraseId = chosenChoicePhrase.Id;

            var choicesFromTheSamePhrase = testcaseData.DialogChoices.Where(_ => _.PhraseId == chosenChoicePhrase.Id && chosenChoicePhrase.Id != null && (UniqueId)_.DialogId == dialogTemplate.Id);
            DeleteChoicesFromTheSamePhrase(testcaseData, choicesFromTheSamePhrase, chosenChoiceId);
        }
    }

    void DeleteChoicesFromTheSamePhrase(TestCaseData testCaseData, IEnumerable<Data.AutomaticTestData.DialogChoiceData> choicesFromTheSamePhrase, string chosenChoiceId)
    {
        foreach (var choice in choicesFromTheSamePhrase.ToList())
        {
            if (choice.ChoiceId != chosenChoiceId || testCaseData.DialogChoices.Count(_ => _.ChoiceId == choice.ChoiceId && _.DialogId == choice.DialogId) > 1)
            {
                EditorUtility.DisplayDialog("Invalid choice",
                    "Chosen id '" + chosenChoiceId + "' excludes previously defined choice '" + choice.ChoiceId +
                    "', because they are defined in the same phrase. You can define only one choice from one phrase. Duplicated id will be deleted.", "Ok");
                _choiceToRemove = choice;
            }
        }
    }
}
