﻿using AutomaticExplorationTest;
using EventHorizon.Utils;
using System.Collections.Generic;
using System.Linq;
using Windows.BasicDrawers.Filters;

/// <summary>
/// Table with <see cref="GameplayTestData"/>
/// </summary>
public class GameplayTestsDataDrawer : BaseTableDrawer<GameplayTestsDataDrawer, GameplayTestData>
{
    public delegate ITableDrawer ChildTreeFactory(GameplayTestsDataDrawer parent, GameplayTestData test, FilterProvider filter);

    public GameplayTestsDataDrawer(TableDrawerDesc desc, GameEditorState state, MapName? parentMapName, ChildTreeFactory childTreeFactory)
        : base(desc, _columns, "test", _ => !parentMapName.HasValue || (_.MapName == parentMapName))
    {
        _state = state;
        _childTreeFactory = childTreeFactory;
        _parentMapName = parentMapName;
    }

    public override bool CanSwap(GameplayTestData obj) { return true; }

    public override GameplayTestData CreateNew()
    {
        var test = new GameplayTestData(
            new UniqueObjectDesc(typeof(TestsTemplatesData), UniqueId.NewFromGuid("test_")),
            "TestName",
            _parentMapName.Value,
            _state.Storage
        );

        GameEditorUtils.ActiveState.Storage.TestsTemplatesData.Tests.Add(test);
        return test;
    }

    public override IEnumerable<GameplayTestData> GetItems()
    {
        return _state.Storage.TestsTemplatesData.Tests.Values.OrderBy(_ => _.PassId).ThenBy(_ => _.Order);
    }

    public override void DrawChildren(GameplayTestData obj, float width)
    {
        var drawer = GetOrCreateChild(obj);
        drawer.Draw(width);
    }

    protected override bool OnSwap(GameplayTestData a, GameplayTestData b)
    {
        return _state.Storage.TestsTemplatesData.SwapTestsOrder(a, b);
    }

    protected override void OnRemove(GameplayTestData obj)
    {
        obj.Remove(_state.Storage);
    }

    protected override MatchResult IsItemMatching(GameplayTestData item, IContentFilter filter)
    {
        if (filter.IsMatching(item.Id)
            || filter.IsMatching(item.Name))
        {
            return MatchResult.Self;
        }

        return GetOrCreateChild(item).HasAnyItemMatching(filter) ? MatchResult.Child : MatchResult.None;
    }

    protected override void OnClearCachedItems()
    {
        _childDrawers.ForEach(_ => _.Value.ClearCachedItems());
    }

    protected override void OnChange(BaseColumn column, GameplayTestData testData)
    {
        testData.OnDataChanged(_state.Storage, isPassIdChanged: column.Label == _passIdColumnLabel);
    }

    readonly GameEditorState _state;
    readonly static string _passIdColumnLabel = "PassId";
    readonly MapName? _parentMapName;
    readonly ChildTreeFactory _childTreeFactory;
    readonly Dictionary<GameplayTestData, ITableDrawer> _childDrawers = new Dictionary<GameplayTestData, ITableDrawer>();

    static readonly BaseColumn[] _columns = {
        CreateOrderColumn(),
        CreateDetailRequestColumn(),

        new DefaultColumn(){ Label = "Test", FixedWidth = 30, IsVisible = true, DrawHandler = _ => EditBool(_.Value, "ShouldBeProcessed", _.Size)},
        new DefaultColumn(){ Label = _passIdColumnLabel, FixedWidth = 50, IsVisible = true, ShouldNotifyAboutChange = true, DrawHandler = _ => EditInt(_.Value, "PassId", _.Size)},
        new DefaultColumn(){ Label = "Order", FixedWidth = 40, IsVisible = true, DrawHandler = _ => DrawLabel(_.Value.Order.ToString(),_.Size)},
        new DefaultColumn(){ Label = "Name", FixedWidth = 200, IsVisibleInDetailView = false, DrawHandler = _ => DrawLabel(_.Value.Name.ToString(), _.Size, BoldLabelStyle) },

        new DefaultColumn(){ Label = "Edit Name", FixedWidth = 200, IsVisibleOnlyInDetailView = false, DrawHandler = _ => EditString(_.Value, "Name", _.Size)},
        new DefaultColumn(){ Label = "IsValid", FixedWidth = 200, IsVisibleOnlyInDetailView = true, DrawHandler = _ => DrawLabel(_.Value.IsValid.ToString(),_.Size)},
        new DefaultColumn(){ Label = "Map", FixedWidth = 150, IsVisibleOnlyInDetailView = true, ShouldNotifyAboutChange = true,
            ValueHandler = _ => _.MapName,
            CompareHandler = (a, b) => a.MapName.CompareTo(b.MapName),
            DrawHandler = _ => EditEnum(_.Value, "MapName", GameEditorUtils.ExplorationMaps, GameEditorUtils.ExplorationMapsLabels, _.Size) },
        new DefaultColumn(){ Label = "TestLog", FixedWidth = 500, IsVisibleOnlyInDetailView = true, DrawHandler = _ => DrawGameplayTestLog(_.Value) },
    };

    ITableDrawer GetOrCreateChild(GameplayTestData test)
    {
        return _childDrawers.GetOrCreate(test, () => _childTreeFactory(this, test, ContentFilter));
    }
}
