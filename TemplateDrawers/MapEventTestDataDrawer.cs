﻿using AutomaticExplorationTest;
using Data.AutomaticTestData;
using System.Collections.Generic;
using UnityEngine;
using Windows.BasicDrawers.Filters;

/// <summary>
/// Table with <see cref="MapEventTestData"/>
/// </summary>
public class MapEventTestsTemplatesDrawer : BaseTableDrawer<MapEventTestsTemplatesDrawer, MapEventTestData>
{
    public MapEventTestsTemplatesDrawer(TableDrawerDesc desc, TestCaseData testCase, GameEditorState state)
        : base(desc, _columns, "mapEventTest")
    {
        _state = state;
        _testCase = testCase;
        _validMapName = testCase.MapName;
    }

    public override bool ShowExpand(MapEventTestData obj) { return false; }

    public override MapEventTestData CreateNew()
    {
        var newMapEventTest = new MapEventTestData(
            new UniqueObjectDesc(typeof(TestsTemplatesData), UniqueId.NewFromGuid("mapEventTest_")),
            MapEventExpectedState.None,
            MapEventExpectedState.None,
            "MapEventReactionName",
            _testCase
        );

        GameEditorUtils.ActiveState.Storage.TestsTemplatesData.MapEventsTests.Add(newMapEventTest);

        return newMapEventTest;
    }

    public override IEnumerable<MapEventTestData> GetItems()
    {
        return _testCase.TestConditionsData.OfTypeSafe<ITestConditionData<TestCaseData>, MapEventTestData>();
    }

    protected override void OnChange(BaseColumn column, MapEventTestData mapEventTestData)
    {
        _testCase.Test.OnDataChanged(_state.Storage);
    }

    protected override void OnClearCachedItems() { }

    protected override MatchResult IsItemMatching(MapEventTestData item, IContentFilter filter)
    {
        return (filter.IsMatching(item.Id) || filter.IsMatching(item.Name)) ? MatchResult.Self : MatchResult.None;
    }

    protected override void OnRemove(MapEventTestData obj)
    {
        if (!_testCase.TryRemoveTestCondition(obj))
        {
            Debug.LogError("Couldn't remove reaction");
        }
    }

    readonly TestCaseData _testCase;
    readonly GameEditorState _state;
    static MapName _validMapName;

    static readonly BaseColumn[] _columns = {
        CreateOrderColumn(),
        CreateDetailRequestColumn(),

        new DefaultColumn(){ Label = "Name", FixedWidth = 200, IsVisibleInDetailView = false, DrawHandler = _ => DrawLabel(_.Value.Name.ToString(), _.Size, BoldLabelStyle) },
        new DefaultColumn(){ Label = "Edit Name", FixedWidth = 200, IsVisibleOnlyInDetailView = false, DrawHandler = _ => EditString(_.Value, "Name", _.Size)},
        new DefaultColumn(){ Label = "MapEvent", FixedWidth = 200, IsVisibleInDetailView = true, ShouldNotifyAboutChange = true, DrawHandler = _ => DrawMapEventField(_.Value, _validMapName, _.Size, isTestCaseTarget: false) },
        new DefaultColumn(){ Label = "IsValid", FixedWidth = 200, IsVisibleOnlyInDetailView = true, DrawHandler = _ => DrawLabel(_.Value.IsValid.ToString(),_.Size)},
        new DefaultColumn(){ Label = "Precondition", FixedWidth = 200, IsVisibleInDetailView = true, ShouldNotifyAboutChange = true, DrawHandler = _ => EditEnum<MapEventExpectedState>(_.Value, "PreCondition", _.Size ) },
        new DefaultColumn(){ Label = "Postcondition", FixedWidth = 200, IsVisibleInDetailView = true, ShouldNotifyAboutChange = true, DrawHandler = _ => EditEnum<MapEventExpectedState>(_.Value, "PostCondition", _.Size ) },
        CreateColumnWithCallbackButton("Saves", 60, false, (_) => TestEditorUtils.ShowTestConditionSaves(_.Value)),
    };
}
