﻿using System;
using System.Collections.Generic;
using AutomaticExplorationTest;
using Windows.BasicDrawers.Filters;

/// <summary>
/// All supported types of conditions to test
/// </summary>
public enum TestCaseConditionType
{
    MapEvent,
    TemplateId
}

/// <summary>
/// Table with <see cref="MapEventTestData"/>
/// </summary>
public class TestCaseConditionsDataDrawer : GroupedTablesDrawer<TestCaseConditionType>
{
    public TestCaseConditionsDataDrawer(TableDrawerDesc desc, GameEditorState state,
        IEnumerable<TestCaseConditionType> items, Func<TestCaseConditionType, string> getName, TestCaseData testCaseData)
        : base(desc, state, new FilterProvider(), "", items, getName, null)
    {
        _testCaseData = testCaseData;
        _state = state;
    }

    public override void DrawChildren(TestCaseConditionType obj, float width)
    {
        var drawer = GetOrCreateChild(obj);
        drawer.Draw(width);
    }

    protected override MatchResult IsItemMatching(TestCaseConditionType item, IContentFilter filter)
    {
        return GetOrCreateChild(item).HasAnyItemMatching(filter) ? MatchResult.Child : MatchResult.None;
    }

    readonly TestCaseData _testCaseData;
    private GameEditorState _state;

    ITableDrawer GetOrCreateChild(TestCaseConditionType conditionType)
    {
        switch (conditionType)
        {
            case TestCaseConditionType.MapEvent: return new MapEventTestsTemplatesDrawer(Desc, _testCaseData, _state);
            case TestCaseConditionType.TemplateId: return new TemplateIdTestTemplatesDrawer(Desc, _testCaseData, _state);
            default:
                return null;
        }
    }
}
