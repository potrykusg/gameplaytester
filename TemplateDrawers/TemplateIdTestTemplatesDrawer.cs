﻿using AutomaticExplorationTest;
using Data.AutomaticTestData;
using System;
using System.Collections.Generic;
using System.Linq;
using Data.AutomaticTestData.TemplateIdConditions;
using UnityEditor;
using UnityEngine;
using Windows.BasicDrawers.Filters;

/// <summary>
/// Table with <see cref="TemplateIdTestData"/>
/// </summary>
public class TemplateIdTestTemplatesDrawer : BaseTableDrawer<TemplateIdTestTemplatesDrawer, TemplateIdTestData>
{
    /// <summary>
    /// Container for condition drawer used by <see cref="TemplateIdTestTemplatesDrawer"/>
    /// </summary>
    public class ConditionDrawer
    {
        public ITemplateIdValidationDrawer Drawer { get; private set; }

        public ConditionDrawer(ITemplateIdValidationDrawer drawer, Func<TemplateIdTestData, ITemplateIdCondition> creator, Action<ItemInfo, string> editPropHandler)
        {
            Drawer = drawer;
            _creator = creator;
            _propEditor = editPropHandler;
        }

        public void Edit(ItemInfo itemInfo, string name)
        {
            _propEditor(itemInfo, name);
        }

        public ITemplateIdCondition Create(TemplateIdTestData testData)
        {
            return _creator(testData);
        }

        public bool IsValidFor(string templateId)
        {
            string message;
            MessageType type;
            Drawer.ValidateId(templateId, out message, out type);
            return type != MessageType.Error;
        }

        readonly Func<TemplateIdTestData, ITemplateIdCondition> _creator;
        readonly Action<ItemInfo, string> _propEditor;
    }

    public TemplateIdTestTemplatesDrawer(TableDrawerDesc desc, TestCaseData testCase, GameEditorState state)
        : base(desc, _columns, "uniqueIdTest")
    {
        _testCase = testCase;
        _state = state;
    }

    public override bool ShowExpand(TemplateIdTestData obj) { return false; }

    public override TemplateIdTestData CreateNew()
    {
        var newQuestTaskTest = new TemplateIdTestData(
            new UniqueObjectDesc(typeof(TestsTemplatesData), UniqueId.NewFromGuid("uniqueIdTest_")),
            "IdTest",
            _testCase
        );

        _state.Storage.TestsTemplatesData.TemplateIdTests.Add(newQuestTaskTest);

        return newQuestTaskTest;
    }

    public override IEnumerable<TemplateIdTestData> GetItems()
    {
        return _testCase.TestConditionsData.OfType<TemplateIdTestData>();
    }

    protected override void OnClearCachedItems() { }

    protected override MatchResult IsItemMatching(TemplateIdTestData item, IContentFilter filter)
    {
        return (filter.IsMatching(item.Id) || filter.IsMatching(item.TemplateId) || filter.IsMatching(item.Name)) ? MatchResult.Self : MatchResult.None;
    }

    protected override void OnRemove(TemplateIdTestData obj)
    {
        if (!_testCase.TryRemoveTestCondition(obj))
        {
            Debug.LogError("Couldn't remove reaction");
        }
    }

    protected override void OnChange(BaseColumn column, TemplateIdTestData questTaskTestData)
    {
        _testCase.Test.OnDataChanged(_state.Storage);
    }

    readonly TestCaseData _testCase;
    readonly GameEditorState _state;
    static readonly Dictionary<TemplateIdTestData, ConditionDrawer> _conditionDrawersByTest = new Dictionary<TemplateIdTestData, ConditionDrawer>();

    static readonly ConditionDrawer[] _conditionDrawers = new[]
    {
        new ConditionDrawer(MovieKeyIdSelectorDrawer.Ref, _ => new MovieIdCondition(_), (itemInfo, name) => EditEnum<MovieExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(UnitTemplateIdSelectorDrawer.Ref, _ => new UnitIdCondition(_), (itemInfo, name) => EditEnum<UnitExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(TaskTemplateIdSelectorDrawer.Ref, _  => new QuestTaskIdCondition(_) , (itemInfo, name) => EditEnum<QuestTaskExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(QuestTemplateIdSelectorDrawer.Ref, _  => new QuestIdCondition(_), (itemInfo, name) => EditEnum<QuestExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(ItemTemplateIdSelectorDrawer.Ref, _  => new InventoryItemIdCondition(_), (itemInfo, name) => EditEnum<InventoryItemExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(MapTemplateIdSelectorDrawer.Ref, _  => new MapIdCondition(_), (itemInfo, name) => EditEnum<ExplorationMapExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size)),
        new ConditionDrawer(ItemUniqueIdSelectorDrawer.Ref, _  => new InventoryItemIdCondition(_), (itemInfo, name) => EditEnum<InventoryItemExpectedState>(itemInfo.Value.TemplateIdCondition, name,itemInfo.Size))     
    };

    static void UpdateTemplateConditionType(TemplateIdTestData testData)
    {
        if (!_conditionDrawersByTest.ContainsKey(testData))
        {
            _conditionDrawersByTest.Add(testData, _conditionDrawers.FirstOrDefault(_ => _.IsValidFor(testData.TemplateId)));
        }

        var testConditionDrawer = _conditionDrawersByTest[testData];

        if (testConditionDrawer == null || !testConditionDrawer.IsValidFor(testData.TemplateId))
        {
            testConditionDrawer = _conditionDrawers.FirstOrDefault(_ => _.IsValidFor(testData.TemplateId));
            _conditionDrawersByTest[testData] = testConditionDrawer;
        }
        else
        {
            return;
        }

        if (testConditionDrawer == null)
        {
            return;
        }

        testData.TemplateIdCondition = testConditionDrawer.Create(testData);
    }

    static void EditCondition(ItemInfo itemInfo, bool isPreCondition)
    {
        var conditionCreator = _conditionDrawersByTest[itemInfo.Value];

        if (conditionCreator == null)
        {
            DrawLabel("Wrong Id", itemInfo.Size, InvalidBackgroundStyle);
            return;
        }

        string name = isPreCondition ? "PreCondition" : "PostCondition";
        conditionCreator.Edit(itemInfo, name);
    }

    static readonly BaseColumn[] _columns = new BaseColumn[]
    {
        CreateOrderColumn(),
        CreateDetailRequestColumn(),

        new DefaultColumn(){ Label = "Name", FixedWidth = 200, IsVisibleInDetailView = false, DrawHandler = _ => DrawLabel(_.Value.Name.ToString(), _.Size, BoldLabelStyle) },
        new DefaultColumn(){ Label = "Edit Name", FixedWidth = 200, IsVisibleOnlyInDetailView = false, DrawHandler = _ => EditString(_.Value, "Name", _.Size)},
        new DefaultColumn()
        {
            Label = "TemplateId", FixedWidth = 200, ShouldNotifyAboutChange = true, DrawHandler = _ =>
            {
                EditString(_.Value, "TemplateId", _.Size);
                UpdateTemplateConditionType(_.Value);
            }
        },
        new DefaultColumn(){Label = "PreCondition", FixedWidth = 200, ShouldNotifyAboutChange = true, IsVisibleInDetailView = true, DrawHandler = _ => EditCondition(_, isPreCondition: true) },
        new DefaultColumn(){Label = "PostCondition", FixedWidth = 200, ShouldNotifyAboutChange = true, IsVisibleInDetailView = true, DrawHandler = _ => EditCondition(_, isPreCondition: false)  },

        CreateColumnWithCallbackButton("Saves", 60, false, (_) => TestEditorUtils.ShowTestConditionSaves(_.Value)),

        new DefaultColumn()
        {
            Label = "IdValidationInfo", DrawHandler = _ =>
            {
                var creator = _conditionDrawersByTest[_.Value];
                if (creator == null)
                {
                    DrawLabel("Template id is invalid or not supported", _.Size, InvalidBackgroundStyle);
                    return;
                }

                var drawer = creator.Drawer;
                if (drawer != null)
                {
                    DrawIdValidation(_.Size, _.Value.TemplateId, drawer);
                    return;
                }

                DrawLabel("Template id is invalid or not supported", _.Size, InvalidBackgroundStyle);
            }
        },
    };
}
