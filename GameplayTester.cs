﻿using AutomaticExplorationTest;
using Configuration;
using Data.AutomaticTestData;
using EventHorizon.AutomaticExplorationTest.TestingErrors;
using EventHorizon.Configuration;
using EventHorizon.ExternalServices;
using EventHorizon.GameServices;
using EventHorizon.UI;
using EventHorizon.Utils.TimeLog;
using PlayMakerServices;
using QuestSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR || ENABLE_TESTS
namespace TestServices
{
    /// <summary>
    /// Creates, initializes and validates all defined tests. Runs automatically when appropiate trigger is set.
    /// </summary>
    public class GameplayTester : UniversalServiceSingleton<GameplayTester>
    {
        public const string TestsExecutionCommand = "-runTests";
        public const string LogErrorsDirName = "testingErrorLogs";
        public const float BattleAutoWinTimeout = 300;
        public const float BattleTestTimeScale = 3;

        public string BuildDataPath { get { return Path.Combine(Application.dataPath, "testsTemplatesData.json"); } }
        public float CurrentTestingSessionLength
        {
            get { return _stopWatch.ElapsedMiliseconds / 1000; }
        }

        public string ErrorsLogDataPath
        {
            get
            {
                string dataPath;
#if UNITY_EDITOR
                dataPath = EditorConfig.DataPath;
#else
                dataPath = Application.dataPath;
#endif
                return Path.Combine(dataPath, LogErrorsDirName);
            }
        }

        public string CurrentDataFullPath
        {
            get
            {
#if UNITY_EDITOR
                return PathUtils.Combine(Application.dataPath, "..", EditorConfig.TestsTemplatesDataFilePath);
#else
                return BuildDataPath;
#endif
            }
        }

        public string CurrentDataPath
        {
            get
            {
#if UNITY_EDITOR
                return EditorConfig.TestsTemplatesDataFilePath;
#else
                return BuildDataPath;
#endif
            }
        }

        public bool IsBusy { get; private set; }

        public void Run()
        {
            SetupWorkflow();
            InitTests();
            RunTests();
        }

        public IEnumerator EnsureExplorationMapIsLoaded(MapName mapName)
        {
            yield return WaitForGameModeInited();

            if (_gameManager.CurrentMap == mapName)
            {
                yield break;
            }

            TestLogger.Log("Loading scene " + mapName);

            if (mapName == MapName.Invalid)
            {
                TestLogger.LogError("Invalid map name can't be loaded.");
                yield break;
            }

            yield return LoadExploration(mapName);

            yield return WaitForGameModeInited();
            TestLogger.Log("Scene " + mapName + " inited");
        }

        public void Restart()
        {
            _workflowSetup.Disable();

            if (IsBusy)
            {
                _uiService.ExecuteIfConfirmed("Testing is not finished yet, are you sure you want to restart testing ?", onConfirmed: () =>
                {
                    InternalCancel();
                    InternalRestart();
                }, onCancel: () => _workflowSetup.Enable());
                return;
            }

            InternalRestart();
        }

        public void Cancel()
        {
            if (!IsBusy)
            {
                _automaticTestsPanel.Hide();
                return;
            }

            _workflowSetup.Disable();

            int leftTestsCount = _currentlyProcessedTests.Count + 1;

            _uiService.ExecuteIfConfirmed("Testing is not finished yet, there are still " + leftTestsCount + " tests to process, are you sure you want to interrupt ?", onConfirmed: InternalCancel, onCancel: () => _workflowSetup.Enable());
        }

        protected override void OnAwake()
        {
            base.OnAwake();

#if UNITY_EDITOR
            _isInBuildTestMode = false;
#elif ENABLE_TESTS
            var args = Environment.GetCommandLineArgs();
            if (args.Contains(TestsExecutionCommand))
            {
                _isInBuildTestMode = true;
            }
#endif
            _mapStateService = MapStateService.Ref;
            _fileService = FileService.Ref;
            _dialogService = DialogService.Ref;
            _uiService = UIService.Ref;
            _gameDataStorage = GameDataStorage.Ref;
            _gameManager = GameManager.Ref;
            _panelStackService = UIPanelsStackService.Ref;
            _exploreUnitsManager = ExploreUnitsManager.Ref;
            _playMakerStateController = PlayMakerStateController.Ref;
            _battleBoard = BattleBoard.Ref;
            _partyManager = PartyManager.Ref;
            _inventory = Inventory.Ref;
            _questManager = QuestManager.Ref;
            _translateService = TranslateService.Ref;
            var timeManager = TimeManager.Ref;
            var movieManager = MovieManager.Ref;
            _workflowSetup = new TestWorkflowSetup(_exploreUnitsManager, _uiService, _panelStackService, timeManager, _battleBoard, movieManager);
            _automaticTestsPanel = _uiService.AutomaticTestsPanel;
            _processManager = ProcessManager.Ref;
            _errorsCollector = new ErrorsCollector(_gameManager, _battleBoard);
            _discordBot = DiscordBotService.Ref;


            if (RunTriggerEnabled || _isInBuildTestMode)
            {
                DisableRunTrigger();
#if UNITY_EDITOR
                CustomDialogDisplayer.SkipAllDialogs = true;
#endif
                IsBusy = true;
                _runOnStart = true;
            }
        }

        protected void Start()
        {
            if (_runOnStart)
            {
                Run();
            }
        }

        MapStateService _mapStateService;
        FileService _fileService;
        DialogService _dialogService;
        BattleBoard _battleBoard;
        UIPanelsStackService _panelStackService;
        GameDataStorage _gameDataStorage;
        GameManager _gameManager;
        ProcessManager _processManager;
        UIService _uiService;
        ExploreUnitsManager _exploreUnitsManager;
        QuestManager _questManager;
        TranslateService _translateService;
        Inventory _inventory;
        PartyManager _partyManager;
        PlayMakerStateController _playMakerStateController;
        TestWorkflowSetup _workflowSetup;
        UIAutomaticTestsPanel _automaticTestsPanel;
        readonly StopwatchWatcher _stopWatch = new StopwatchWatcher();

        readonly Queue<GameplayTest> _currentlyProcessedTests = new Queue<GameplayTest>();
        readonly List<GameplayTestData> _failedTestsData = new List<GameplayTestData>();
        GameplayTest _currentTest;
        TestCase _currentTestCase;
        List<GameplayTestData> _allTestsData;
        List<GameplayTestData> _testsDataToProcess;
        Coroutine _testingRoutine;
        Coroutine _progressUpdateRoutine;
        int _currentTestNumber;
        int _currentTestConditionNumber;
        int _currentTestCaseNumber;
        int _allTestsCount;
        int _currentlyProcessedTestsVersion;
        bool _hasAnyTestsFromOtherVersionsToProcess;
        bool _runOnStart;
        string _progressInfo;
        bool RunTriggerEnabled { get { return _gameDataStorage.TestsTemplatesData.TriggerTestRun; } }
        int CurrentlyProcessedTestsVersion { get { return _gameDataStorage.TestsTemplatesData.CurrentlyProcessedTestsVersion; } }
        bool _isInBuildTestMode;
        bool _criticalErrorOccured;
        ErrorsCollector _errorsCollector;
        DiscordBotService _discordBot;

        void InternalRestart()
        {
            _stopWatch.Reset();
            _failedTestsData.Clear();
            _gameDataStorage.LoadTestsTemplates();
            _currentTestCaseNumber = 0;
            _currentTestConditionNumber = 0;
            _currentTestNumber = 0;
            _gameManager.LoadNewGame(GameDifficultyMode.Normal);
            Run();
        }

        void InternalCancel()
        {
            Interrupt();
            _automaticTestsPanel.Hide();
            Finish();
        }

        void SetCurrentlyProcessedTestsVersion(int version)
        {
            _gameDataStorage.TestsTemplatesData.CurrentlyProcessedTestsVersion = version;
        }

        void SetupWorkflow()
        {
            IsBusy = true;
            _workflowSetup.Enable();
        }

        void Stop()
        {
            if (_currentTestCase.IsValidating)
            {
                _currentTestCase.OnInterrupted();
            }

            _stopWatch.Stop();
            _currentTest = null;
            _currentTestCase = null;
            ClearProgress();
            StopAllCoroutines();

            _workflowSetup.Disable();
            IsBusy = false;
        }

        void ClearProgress()
        {
            CoroutineUtils.TryStopAndClear(ref _progressUpdateRoutine, this);
        }

        void DisableRunTrigger()
        {
            _gameDataStorage.TestsTemplatesData.TriggerTestRun = false;
            _gameDataStorage.SaveTests();
        }

        void RunTests()
        {
            //TODO: keep this value in json in order to support counting length of tests from another modes (restarting app)
            _stopWatch.Reset();
            _stopWatch.Start();
            _testingRoutine = StartCoroutine(StartTesting());
            _discordBot.UploadMessage("Testing started", TestLogType.Start);
        }

        IEnumerator StartTesting()
        {
            //TODO: add testing time log
            Application.logMessageReceived += HandleErrorsAndExceptions;
            //TODO: Catch exceptions here, except during test case, to ensure that exceptions thrown between tests will be also catched
            _progressInfo = "Starting...";
            CoroutineUtils.Restart(ref _progressUpdateRoutine, UpdateProgressInfoCoroutine(() => _progressInfo, _currentlyProcessedTestsVersion), this);
            _automaticTestsPanel.Show();
            yield return WaitForGameModeInited();
            yield return ValidateTests();
        }

        void HandleErrorsAndExceptions(string message, string stackTrace, LogType logType)
        {
            _errorsCollector.Collect(message, stackTrace, logType);
            TestLogger.LogErrorOrException(message, stackTrace, logType);
        }

        void InitTests()
        {     
            var allTestsData = _gameDataStorage.TestsTemplatesData.Tests.Values
               .OrderBy(_ => _mapStateService.GetMapTemplateData(_.MapName).Order)
               .ThenBy(_ => _.PassId)
               .ThenBy(_ => _.Order);

            _allTestsData = _isInBuildTestMode ? allTestsData.ToList() : allTestsData.Where(_ => _.ShouldBeProcessed).ToList();

            if (_allTestsData.All(_ => _.PassId != CurrentlyProcessedTestsVersion))
            {
                SetCurrentlyProcessedTestsVersion(_allTestsData.Select(_ => _.PassId).Min());
            }

            _testsDataToProcess = _allTestsData
                .Where(_ => _.PassId == CurrentlyProcessedTestsVersion)
                .ToList();

            _allTestsCount = _allTestsData.Count;
            _currentlyProcessedTests.Clear();

            foreach (var testTemplate in _testsDataToProcess)
            {
                InitTest(testTemplate);
            }
        }

        void CriticalErrorHandler(string errorMessage)
        {
            _criticalErrorOccured = true;
            string fullErrorMessage = "Testing is interrupted due to critical error: " + errorMessage;
            TestLogger.LogError(fullErrorMessage);

            if (_currentTest != null)
            {
                SaveLogsToTest(_currentTest.Data);
            }

            _discordBot.UploadMessage(fullErrorMessage, TestLogType.Negative);
            _uiService.ShowErrorPopup("Error", fullErrorMessage);
            Finish();
        }

        void InitTest(GameplayTestData template)
        {
            var test = new GameplayTest(this, CriticalErrorHandler, template, _gameDataStorage, _gameManager, _battleBoard, _playMakerStateController, _uiService,
                _dialogService, _fileService, _questManager, _translateService, _inventory, _partyManager);

            _currentlyProcessedTests.Enqueue(test);
        }

        void OnTestCaseValidationStarted(TestCase testCase)
        {
            _currentTestCase = testCase;
            _currentTestCaseNumber++;
        }

        void OnTestCaseValidationFinished(TestCase testCase)
        {
            _currentTestConditionNumber += testCase.Data.TestConditionsData.Count();
        }

        IEnumerator ValidateTests()
        {
            _currentlyProcessedTestsVersion = _gameDataStorage.TestsTemplatesData.CurrentlyProcessedTestsVersion;
            var alreadyProcessedTests = _allTestsData.Where(_ => _.PassId < _currentlyProcessedTestsVersion);

            if (!alreadyProcessedTests.IsNullOrEmpty())
            {
                _failedTestsData.AddRange(alreadyProcessedTests.Where(_ => !_.IsValid));
                _currentTestNumber += alreadyProcessedTests.Count();
                _currentTestCaseNumber += alreadyProcessedTests.Select(_ => _.TestCasesData.Count()).Sum();
                _currentTestConditionNumber += alreadyProcessedTests
                    .SelectMany(_ => _.TestCasesData)
                    .SelectMany(_ => _.TestConditionsData).Count();
            }

            while (_currentlyProcessedTests.Any())
            {
                var prevTest = _currentTest;
                _currentTest = _currentlyProcessedTests.Dequeue();

                if (prevTest != null && prevTest.Data.MapName != _currentTest.Data.MapName)
                {
                    _discordBot.UploadMessage("Finished tests at " + prevTest.Data.MapName);
                }

                _currentTestNumber++;
                var currentTestData = _currentTest.Data;

                TestLogger.ClearLog();
                LogCurrentDateAndTime();
                TestLogger.LogImportant("Test: " + currentTestData.Name);

                if (currentTestData.MapName != _gameManager.CurrentMap)
                {
                    yield return EnsureExplorationMapIsLoaded(_currentTest.Data.MapName);
                }

                _currentTest.ResetData();

                yield return WaitForGameModeInited();

                _progressInfo = "Test " + _currentTestNumber + " / " + _allTestsCount;

                yield return _currentTest.Validate(this, OnTestCaseValidationStarted, OnTestCaseValidationFinished);

                string testResult;

                if (_currentTest.IsValid)
                {
                    testResult = "Test " + currentTestData.Name + " passed";
                    TestLogger.LogPositive(testResult);
                }
                else
                {
                    testResult = "[" + currentTestData.MapName + "] " + "Test " + currentTestData.Name + " failed";
                    TestLogger.LogError(testResult);
                    _discordBot.UploadMessage(testResult, TestLogType.Negative);
                }

                SaveLogsToTest(_currentTest.Data);
                TestLogger.ClearLog();

                if (!_currentTest.IsValid)
                {
                    _failedTestsData.Add(_currentTest.Data);
                }
            }

            var nextTestsVersion = _allTestsData
                .Where(_ => _.PassId > _currentTest.Data.PassId)
                .Select(_ => _.PassId)
                .DefaultIfEmpty()
                .Min();

            if (nextTestsVersion == 0)
            {
                Finish();
            }
            else
            {
                _discordBot.UploadMessage("Restarting app in order to process tests with version " + nextTestsVersion);
                RunTestsWithAnotherVersion(nextTestsVersion);
            }
        }

        void SaveLogsToTest(GameplayTestData testData)
        {
            LogCurrentDateAndTime();
            testData.Logs = TestLogger.GetWholeLog();
        }

        void RunTestsWithAnotherVersion(int version)
        {
            _hasAnyTestsFromOtherVersionsToProcess = true;
            _gameDataStorage.TestsTemplatesData.CurrentlyProcessedTestsVersion = version;
            _gameDataStorage.TestsTemplatesData.TriggerTestRun = true;
            _gameDataStorage.SaveTests();
            _gameManager.Quit();
        }

        void OnApplicationQuit()
        {
            ClearProgress();

            if (_hasAnyTestsFromOtherVersionsToProcess)
            {
                _processManager.StartApplication();
            }
        }

        void LogCurrentDateAndTime()
        {
            //TODO: Move logging calls to another class
            TestLogger.Log("Test date " + DateTime.Now.ToShortDateString());
            TestLogger.Log("Test time " + DateTime.Now.ToShortTimeString());
        }

        IEnumerator UpdateProgressInfoCoroutine(Func<string> getProgressInfo, int currentTestsVersion)
        {
            while (IsBusy)
            {
                UpdateProgressInfo(getProgressInfo);
                yield return null;
            }
        }

        void UpdateProgressInfo(Func<string> getProgressInfo)
        {
            var testName = _currentTest == null ? "Loading test..." : "Test: " + _currentTest.Data.Name;
            var testCaseName = _currentTestCase == null ? "Loading test case..." : "TestCase: " + _currentTestCase.Name;
            var allTestCasesCount = _allTestsData.Select(_ => _.TestCasesData.Count()).Sum();
            var allTestsConditionsCount = _allTestsData.SelectMany(_ => _.TestCasesData).Sum(_ => _.TestConditionsData.Count());
            //TODO: Wrap arguments to some struct
            _automaticTestsPanel.UpdateData(testName, testCaseName, _failedTestsData.Select(_ => _.Name).ToArray(), getProgressInfo(), _currentTestCaseNumber, allTestCasesCount, _currentTestConditionNumber, allTestsConditionsCount, _currentlyProcessedTestsVersion);
        }

        void Interrupt()
        {
            var pendingTests = _currentlyProcessedTests.ToList();
            pendingTests.Add(_currentTest);

            foreach (var test in pendingTests)
            {
                test.Data.IsValid = false;
                _failedTestsData.Add(test.Data);
                TestLogger.LogError("Test was interrupted by user.");
                SaveLogsToTest(test.Data);
                TestLogger.ClearLog();
            }
        }

        void Finish()
        {
            Application.logMessageReceived -= HandleErrorsAndExceptions;
            //TODO: automatically upload tests results to google drive. Notify about results

            int failedTestsCount = _failedTestsData.Count;
            var result = "Finished all tests\n" + (_currentTestNumber - failedTestsCount) + " / " + _allTestsCount + " passed.";
            _discordBot.UploadMessage(result, failedTestsCount > 0 || _criticalErrorOccured ? TestLogType.Negative : TestLogType.Positive);

            var releaseDate = new DateTime(2018, 5, 8, 12, 0, 0);
            var timeToRelease = (releaseDate - DateTime.Now).TotalHours;
            _discordBot.UploadMessage("FORTUNETALLY, THERE ARE ONLY " + (int)timeToRelease + " HOURS TO RELEASE PILLARS OF ETERNITY DEADFIRE. SO FINALLY I CAN PLAY SOMETHING GOOD.");

            foreach (GameMode gameMode in Enum.GetValues(typeof(GameMode)))
            {
                var log = _errorsCollector.GenerateLog(gameMode);
                if (log.IsNullOrEmpty())
                {
                    continue;
                }

                var file = _fileService.GetFile(gameMode.ToString(), Config.LogFileExtension, ErrorsLogDataPath);
                if (_fileService.TryWriteFile(file.FullName, Encoding.UTF8.GetBytes(log)))
                {
                    _discordBot.UploadFile(gameMode.ToString(), file.FullName);
                }
            }

            _discordBot.UploadFile("Tests logs", CurrentDataFullPath);

            _gameDataStorage.TestsTemplatesData.CurrentlyProcessedTestsVersion = 0;

            UpdateProgressInfo(() => result);
            Stop();
            _uiService.ShowInfoPopUp(result);
            _gameDataStorage.SaveTests();

            _discordBot.UploadMessage("Testing finished.", TestLogType.Finish);
        }

        IEnumerator LoadExploration(MapName mapName)
        {
            _gameManager.LoadExploration(mapName);
            _progressInfo = "Loading scene " + mapName;
            yield return WaitForGameModeInited();
            _progressInfo = "Test " + _currentTestNumber + " / " + _allTestsCount;
        }

        void ValidateLoadedScenes()
        {
            var currentGameMode = _gameManager.CurrentGameMode;

            if (currentGameMode == GameMode.Exploration || currentGameMode == GameMode.Battle)
            {
                ValidateNavmesh();
            }

            if (_gameManager.CurrentGameMode == GameMode.Exploration)
            {
                ValidateFullScenesCount();
            }
        }

        void ValidateFullScenesCount()
        {
#if UNITY_EDITOR
            if (DebugConfig.SceneLoadingModeInEditor != DebugConfig.SceneLoadingMode.Full)
            {
                return;
            }
#endif
            int loadedScenesCount = SceneManager.sceneCount;
            int loadedFullScenesCount = 0;

            for (int i = 0; i < loadedScenesCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name.EndsWith("_Full"))
                {
                    loadedFullScenesCount++;
                }
            }

            if (loadedFullScenesCount != 1)
            {
                gameObject.LogCustomException("Critical error during scene loading. " + loadedFullScenesCount + " full scenes are loaded in level " + _gameManager.CurrentMap + ", when it should be always exactly one.");
            }
        }

        void ValidateNavmesh()
        {
            var navmeshDatas = Resources.FindObjectsOfTypeAll<NavMeshData>();

            if (navmeshDatas.Length != 1)
            {
                gameObject.LogCustomException("Wrong navmesh amount. There is " + navmeshDatas.Length + " navmesh data currently loaded, when it should be exactly one.");
            }
        }

        IEnumerator WaitForGameModeInited()
        {
            while (!_gameManager.IsGameModeInited)
            {
                yield return null;
            }

            ValidateLoadedScenes();
        }
    }
}
#endif
